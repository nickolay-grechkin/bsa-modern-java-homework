package com.binary_studio.fleet_commander.core.subsystems;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class DefenciveSubsystemImpl implements DefenciveSubsystem {

	static String name;
	static PositiveInteger powerGridConsumption;
	static PositiveInteger capacitorConsumption;
	static PositiveInteger impactReductionPercent;
	static PositiveInteger shieldRegeneration;
	static PositiveInteger hullRegeneration;

	public static DefenciveSubsystemImpl construct(String name, PositiveInteger powerGridConsumption,
			PositiveInteger capacitorConsumption, PositiveInteger impactReductionPercent,
			PositiveInteger shieldRegeneration, PositiveInteger hullRegeneration) throws IllegalArgumentException {
		if (name == null || name.trim().isEmpty()) {
			throw new IllegalArgumentException("Name should be not null and not empty");
		}
		DefenciveSubsystemImpl.name = name;
		DefenciveSubsystemImpl.powerGridConsumption = powerGridConsumption;
		DefenciveSubsystemImpl.capacitorConsumption = capacitorConsumption;
		DefenciveSubsystemImpl.impactReductionPercent = impactReductionPercent;
		DefenciveSubsystemImpl.shieldRegeneration = shieldRegeneration;
		DefenciveSubsystemImpl.hullRegeneration = hullRegeneration;
		return new DefenciveSubsystemImpl();
	}

	@Override
	public PositiveInteger getPowerGridConsumption() {
		return DefenciveSubsystemImpl.powerGridConsumption;
	}

	@Override
	public PositiveInteger getCapacitorConsumption() {
		return DefenciveSubsystemImpl.capacitorConsumption;
	}

	@Override
	public String getName() {
		return DefenciveSubsystemImpl.name;
	}

	@Override
	public AttackAction reduceDamage(AttackAction incomingDamage) {
		// Impact reduction percent can't be more than 95
		if (impactReductionPercent.value() > 95) {
			impactReductionPercent = PositiveInteger.of(95);
		}
		double reducedDamage = incomingDamage.damage.value()
				- (int) (double) ((incomingDamage.damage.value() * impactReductionPercent.value()) / 100);
		// If reduced damage < 1, reduce at least 1 damage
		if (reducedDamage < 1) {
			reducedDamage = 1;
		}
		return new AttackAction(PositiveInteger.of((int) Math.round(reducedDamage)), incomingDamage.attacker,
				incomingDamage.target, incomingDamage.weapon);
	}

	@Override
	public RegenerateAction regenerate() {
		return new RegenerateAction(shieldRegeneration, hullRegeneration);
	}

	@Override
	public PositiveInteger getShieldRegeneration() {
		return shieldRegeneration;
	}

	@Override
	public PositiveInteger getHullRegeneration() {
		return hullRegeneration;
	}

}
