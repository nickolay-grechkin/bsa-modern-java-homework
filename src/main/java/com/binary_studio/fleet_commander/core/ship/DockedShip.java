package com.binary_studio.fleet_commander.core.ship;

import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.exceptions.InsufficientPowergridException;
import com.binary_studio.fleet_commander.core.exceptions.NotAllSubsystemsFitted;
import com.binary_studio.fleet_commander.core.ship.contract.ModularVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class DockedShip implements ModularVessel {

	static String name;
	static Integer pgChecker;
	static PositiveInteger shieldHP;
	static PositiveInteger hullHP;
	static PositiveInteger powerGridOutput;
	static PositiveInteger capacitorAmount;
	static PositiveInteger capacitorRechargeRate;
	static PositiveInteger speed;
	static PositiveInteger size;
	static AttackSubsystem attackSubsystem = null;
	static DefenciveSubsystem defenciveSubsystem = null;

	boolean isAttackSystem = false;

	boolean isDefenceSystem = false;

	public static DockedShip construct(String name, PositiveInteger shieldHP, PositiveInteger hullHP,
			PositiveInteger powerGridOutput, PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate,
			PositiveInteger speed, PositiveInteger size) {
		DockedShip.name = name;
		DockedShip.shieldHP = shieldHP;
		DockedShip.hullHP = hullHP;
		DockedShip.powerGridOutput = powerGridOutput;
		DockedShip.capacitorAmount = capacitorAmount;
		DockedShip.capacitorRechargeRate = capacitorRechargeRate;
		DockedShip.speed = speed;
		DockedShip.size = size;
		pgChecker = powerGridOutput.value();
		return new DockedShip();
	}

	@Override
	public void fitAttackSubsystem(AttackSubsystem subsystem) throws InsufficientPowergridException {
		if (subsystem != null) {
			// If pg quantity enough for subsystem, initialize subsystem, if not throw
			// exception
			pgChecker -= subsystem.getPowerGridConsumption().value();
			if (pgChecker < 0) {
				throw new InsufficientPowergridException(pgChecker * -1);
			}
			else {
				attackSubsystem = subsystem;
				this.isAttackSystem = true;
			}
		}
		else {
			attackSubsystem = null;
			this.isAttackSystem = false;
		}
	}

	@Override
	public void fitDefensiveSubsystem(DefenciveSubsystem subsystem) throws InsufficientPowergridException {
		if (subsystem != null) {
			// If pg quantity enough for subsystem, initialize subsystem, if not throw
			// exception
			pgChecker -= subsystem.getPowerGridConsumption().value();
			if (pgChecker < 0) {
				throw new InsufficientPowergridException(pgChecker * -1);
			}
			else {
				defenciveSubsystem = subsystem;
				this.isDefenceSystem = true;
			}
		}
		else {
			defenciveSubsystem = null;
			this.isDefenceSystem = false;
		}
	}

	public CombatReadyShip undock() throws NotAllSubsystemsFitted {
		if (!this.isAttackSystem && !this.isDefenceSystem) {
			throw NotAllSubsystemsFitted.bothMissing();
		}
		else if (this.isDefenceSystem && !this.isAttackSystem) {
			throw NotAllSubsystemsFitted.attackMissing();
		}
		else if (!this.isDefenceSystem) {
			throw NotAllSubsystemsFitted.defenciveMissing();
		}
		return new CombatReadyShip();
	}

}
