package com.binary_studio.dependency_detector;

import java.util.HashSet;
import java.util.Set;

public final class DependencyDetector {

	private DependencyDetector() {
	}

	public static boolean canBuild(DependencyList libraries) {
		if (!libraries.libraries.isEmpty()) {
			Set<String> currentDependencies = new HashSet<>();
			for (int i = 0; i < libraries.dependencies.size(); i++) {
				for (int j = 0; j < libraries.dependencies.get(i).length; j++) {
					// If there was already a dependency on this library before, return
					// false
					if (j % 2 != 0 && currentDependencies.contains(libraries.dependencies.get(i)[j])) {
						return false;
					}
					// If there wasn't already a dependency on this library before, return
					// add this library to set
					else if (j % 2 == 0) {
						currentDependencies.add(libraries.dependencies.get(i)[j]);
					}
				}
			}
		}
		return true;
	}

}
