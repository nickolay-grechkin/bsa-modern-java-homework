package com.binary_studio.fleet_commander.core.subsystems;

import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;

public final class AttackSubsystemImpl implements AttackSubsystem {

	static String name;
	static PositiveInteger powerGridRequirements;
	static PositiveInteger capacitorConsumption;
	static PositiveInteger optimalSpeed;
	static PositiveInteger optimalSize;
	static PositiveInteger baseDamage;

	public static AttackSubsystemImpl construct(String name, PositiveInteger powerGridRequirements,
			PositiveInteger capacitorConsumption, PositiveInteger optimalSpeed, PositiveInteger optimalSize,
			PositiveInteger baseDamage) throws IllegalArgumentException {
		if (name == null || name.trim().isEmpty()) {
			throw new IllegalArgumentException("Name should be not null and not empty");
		}
		AttackSubsystemImpl.name = name;
		AttackSubsystemImpl.powerGridRequirements = powerGridRequirements;
		AttackSubsystemImpl.capacitorConsumption = capacitorConsumption;
		AttackSubsystemImpl.optimalSpeed = optimalSpeed;
		AttackSubsystemImpl.optimalSize = optimalSize;
		AttackSubsystemImpl.baseDamage = baseDamage;
		return new AttackSubsystemImpl();
	}

	@Override
	public PositiveInteger getPowerGridConsumption() {
		return powerGridRequirements;
	}

	@Override
	public PositiveInteger getCapacitorConsumption() {
		return capacitorConsumption;
	}

	@Override
	public PositiveInteger attack(Attackable target) {
		double sizeReductionModifier;
		double speedReductionModifier;
		PositiveInteger damage;
		if (target.getSize().value() >= AttackSubsystemImpl.optimalSize.value()) {
			sizeReductionModifier = 1;
		}
		else {
			sizeReductionModifier = (double) target.getSize().value() / AttackSubsystemImpl.optimalSize.value();
		}
		if (target.getCurrentSpeed().value() <= AttackSubsystemImpl.optimalSpeed.value()) {
			speedReductionModifier = 1;
		}
		else {
			speedReductionModifier = (double) AttackSubsystemImpl.optimalSpeed.value()
					/ (2 * target.getCurrentSpeed().value());
		}
		damage = PositiveInteger
				.of((int) Math.round(baseDamage.value() * Math.min(sizeReductionModifier, speedReductionModifier)));
		return damage;
	}

	@Override
	public String getName() {
		return name;
	}

}
