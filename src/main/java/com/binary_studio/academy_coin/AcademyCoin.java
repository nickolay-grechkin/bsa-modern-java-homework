package com.binary_studio.academy_coin;

import java.util.ArrayList;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public final class AcademyCoin {

	private AcademyCoin() {
	}

	public static int maxProfit(Stream<Integer> prices) {
		Integer[] pricesArray = new Integer[0];
		int index;
		ArrayList<Integer> resultArray = new ArrayList<>();
		pricesArray = prices.collect(Collectors.toList()).toArray(pricesArray);
		int profit = 0;
		int lastLessValue = 0;
		for (int i = 0; i < pricesArray.length - 1; i++) {
			// Going throw the array and check if number is less than next one. If true,
			// it mean that we should buy
			// academy coin
			if (pricesArray[i] < pricesArray[i + 1]) {
				index = i + 1;
				// If next one number is end of an array, it means that next loop won't
				// work, and we add this number
				// to result array.
				if (index == pricesArray.length - 1) {
					resultArray.add(pricesArray[i + 1]);
				}
				for (int j = i + 1; j < pricesArray.length - 1; j++) {
					// Going throw the array and check if number is more than next one. If
					// true, it mean that we should
					// buy academy coin
					if (pricesArray[j] >= pricesArray[j + 1]) {
						lastLessValue = pricesArray[j + 1];
						resultArray.add(pricesArray[i]);
						resultArray.add(pricesArray[j]);
						i = j;
						break;
					}
				}
			}
		}
		// If size of result array = 1, it means that prices array is sorted
		if (resultArray.size() == 1) {
			profit = resultArray.get(0) - pricesArray[0];
		}
		// If size of result array is even, it means that array doesn't have sorted end
		else if (resultArray.size() % 2 == 0) {
			for (int i = 1; i < resultArray.size(); i += 2) {
				profit += resultArray.get(i) - resultArray.get(i - 1);
			}
		}
		else {
			resultArray.add(resultArray.size() - 1, lastLessValue);
			System.out.println(resultArray);
			for (int i = 1; i < resultArray.size(); i += 2) {
				profit += resultArray.get(i) - resultArray.get(i - 1);
			}
		}
		return profit;
	}

}
