package com.binary_studio.fleet_commander.core.ship;

import java.util.Optional;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.AttackResult;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.ship.contract.CombatReadyVessel;

public final class CombatReadyShip implements CombatReadyVessel {

	double cap = DockedShip.capacitorAmount.value();

	double hp = DockedShip.hullHP.value();

	double shp = DockedShip.shieldHP.value();

	@Override
	public void endTurn() {
		System.out.println("Ship end turn");
		// When turn finished, ship regenerate capacitor amount
		this.cap += DockedShip.capacitorRechargeRate.value();
		if (this.cap > DockedShip.capacitorAmount.value()) {
			this.cap = DockedShip.capacitorAmount.value();
		}
	}

	@Override
	public void startTurn() {
		System.out.println("Ship start turn");
	}

	@Override
	public String getName() {
		return DockedShip.name;
	}

	@Override
	public PositiveInteger getSize() {
		return DockedShip.size;
	}

	@Override
	public PositiveInteger getCurrentSpeed() {
		return DockedShip.speed;
	}

	@Override
	public Optional<AttackAction> attack(Attackable target) {
		Optional<AttackAction> attackAction;
		if (DockedShip.attackSubsystem.getCapacitorConsumption().value() <= this.cap) {
			attackAction = Optional.of(new AttackAction(DockedShip.attackSubsystem.attack(target), this, target,
					DockedShip.attackSubsystem));
			this.cap -= DockedShip.attackSubsystem.getCapacitorConsumption().value();
		}
		else {
			attackAction = Optional.empty();
		}
		return attackAction;
	}

	@Override
	public AttackResult applyAttack(AttackAction attack) {
		// If ship has shield, it absorbs all damage, if no - part of damage go to hp. If
		// shield = 0, all damage goes to hp
		if (this.shp > 0) {
			this.shp -= DockedShip.defenciveSubsystem.reduceDamage(attack).damage.value();
		}
		else if (this.shp < 0) {
			this.hp -= (this.shp * -1);
		}
		else {
			this.hp -= DockedShip.defenciveSubsystem.reduceDamage(attack).damage.value();
		}

		if (this.hp > 0) {
			return new AttackResult.DamageRecived(attack.weapon,
					DockedShip.defenciveSubsystem.reduceDamage(attack).damage, attack.target);
		}
		return new AttackResult.Destroyed();

	}

	@Override
	public Optional<RegenerateAction> regenerate() {
		Optional<RegenerateAction> regenerateAction;
		// If hp == maximum - regenerate only shield, capacitor quantity must be enough
		// for this action
		if (DockedShip.defenciveSubsystem.getCapacitorConsumption().value() <= this.cap) {
			if (this.hp == DockedShip.hullHP.value() && this.shp != DockedShip.shieldHP.value()) {
				regenerateAction = Optional.of(new RegenerateAction(
						DockedShip.defenciveSubsystem.getShieldRegeneration(), PositiveInteger.of(0)));
				this.cap -= DockedShip.defenciveSubsystem.getCapacitorConsumption().value();
			}
			else if (this.hp != DockedShip.hullHP.value() && this.shp == DockedShip.shieldHP.value()) {
				regenerateAction = Optional.of(new RegenerateAction(PositiveInteger.of(0),
						DockedShip.defenciveSubsystem.getHullRegeneration()));
				this.cap -= DockedShip.defenciveSubsystem.getCapacitorConsumption().value();
			}
			else if (this.hp == DockedShip.hullHP.value() && this.shp == DockedShip.shieldHP.value()) {
				regenerateAction = Optional.of(new RegenerateAction(PositiveInteger.of(0), PositiveInteger.of(0)));
				this.cap -= DockedShip.defenciveSubsystem.getCapacitorConsumption().value();
			}
			else {
				regenerateAction = Optional.of(DockedShip.defenciveSubsystem.regenerate());
				this.cap -= DockedShip.defenciveSubsystem.getCapacitorConsumption().value();
			}
		}
		else {
			regenerateAction = Optional.empty();
		}
		return regenerateAction;
	}

}
